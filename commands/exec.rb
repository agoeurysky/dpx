require_relative 'abstract_command'
require 'open3'

class Exec < AbstractCommand
  def initialize (app)
    super (app)
    @name = 'exec'
    @desc = 'Execute a command inside container'
    @opts += [
        [:user, '-u user', '--user user', 'Execute as {user}'],
        [:www, '-w', '--www-data', 'Execute as www-data']
    ]
  end

  def execute
    super

    project_name = @args[1]

    command = 'docker exec -it'
    if @options[:www]
      command += ' -u www-data'
    elsif @options[:user]
      command += " -u #{@options[:user]}"
    end

    command += ' ' + project_name + ' bash'
    if @options[:verbose]
      @app.write("Execute : #{command}")
    end
    system(command)
  end
end