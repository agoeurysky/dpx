require 'pp'
require 'yaml'

require_relative 'commands/exec'
require_relative 'commands/start'
require_relative 'commands/stop'
require_relative 'commands/restart'
require_relative 'commands/init'
require_relative 'commands/add'
require_relative 'commands/remove'
require_relative 'commands/list'

class Dpx

	attr_reader :name, :commands, :config_path
	attr_writer :name, :args

	def initialize (name)
		@name = name
		@config_path = ENV['HOME'] + '/.dpx.yml'
		@commands = [
			Add.new(self),
			Exec.new(self),
			Init.new(self),
			List.new(self),
			Remove.new(self),
			Restart.new(self),
			Start.new(self),
			Stop.new(self),
		]
		# res = system('docker ps')
		# puts res
	end

	def execute (args)
		@commands.each do |command|
			if command.name == args[0]
				command.define_args(args)
				command.execute
				exit(0)
			end
		end

		self.help
	end

	def write (message)
		puts message
	end

	def write_error (message)
		write("\e[31m#{message}\e[0m")
	end

	def write_success (message)
		write("\e[32m#{message}\e[0m")
	end

	def help
		write (@name)
		@commands.each do |command|
			write ("\e[32m#{command.name}\e[0m" + " : " + command.desc)
		end
	end

	def project_config(key)
		config = YAML.load_file(self.config_path)
		if key
			config['project'][key]
		else
			config['project']
		end
	end
end